import { useState } from "react";
import { Button } from "react-bootstrap";

const WeatherFunction = () =>
{
  const [ weatherInfo, setweatherInfo ] = useState('');
  
  const onLoginChanged = (e: any) => {
    console.log(e.target.value);
    setweatherInfo(e.target.value);

    console.log('wheatherInfo: ' + weatherInfo);
  };
  
  const onGetWeatherClick = ()=> {
      const response = fetch(
        '/WeatherForecast'
      )
      .then((response) => response.json())
      .then((response) => setweatherInfo(JSON.stringify(response)))
      .then(() => console.log('weatherInfo: ' + weatherInfo));
  }

     return (
     <div className="container-fluid align-items-center">
      <Button title="Get weather" onClick={onGetWeatherClick}>Get weather</Button>

      <table cellSpacing="10" cellPadding="0">
      <tr>
          <td>{weatherInfo}</td>
      </tr>
      </table>
      <br></br>
     </div>
     );
}

export default WeatherFunction;
