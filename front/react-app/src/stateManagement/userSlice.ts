import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit'
import { User } from "../models/user";

const initialState: User = {
  login: ''
}

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    userSet: (state, action: PayloadAction<string>) => {
      console.log('reduce ' + action.payload);
      state.login = action.payload;
      console.log('reduce login ' + state.login);
    }
  },
});

// each case under reducers becomes an action
export const { userSet } = userSlice.actions;

export default userSlice.reducer;
