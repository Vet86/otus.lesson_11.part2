import { User } from "../models/user";

export const OrderState = Object.freeze({
    USER_SET: '[USER_STATE] USER_SET'
});


export const Actions = Object.freeze({
    setUser: (login: string) => ({ type: OrderState.USER_SET, payload: login })
});

interface State {
    login: User | null;
}

const initialState: State = {
    login: null
}


const userReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case OrderState.USER_SET:    
            return { ...state, user: action.payload };
        default:
            return state;
    }
};
export default userReducer;