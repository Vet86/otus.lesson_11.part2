import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  Route,
  Link,
  Routes,
  BrowserRouter
} from "react-router-dom";
import AuthFunction from './Components/Auth/Auth';
import HomePageComponent from './Components/HomePage/HomePage';
import NotFoundComponent from './Components/NotFound/NotFound';
import RegisterComponent from './Components/Register/Register';
import WeatherFunction from './Components/Weather/Weather';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<HomePageComponent />} />
        <Route path="page1" element={<AuthFunction />} />
        <Route path="page2" element={<RegisterComponent />} />
        <Route path="page3" element={<WeatherFunction />} />
        <Route path="*" element={<NotFoundComponent />} />
      </Routes>


      <nav>
        <ul>
          <li>
            <Link to={'/page1'}>AuthComponent</Link>
          </li>
          <li>
            <Link to={'/page2'}>RegisterComponent</Link>
          </li>
          <li>
            <Link to={'/page3'}>WeatherFunction</Link>
          </li>
          <li>
            <Link to={'/'}>HomePageComponent</Link>
          </li>
        </ul>
      </nav>

    </BrowserRouter >
  );
}
export default App;
